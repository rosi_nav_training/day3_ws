#include <algorithm>
#include <string>
#include <memory>
#include <cmath>

#include "nav2_core/exceptions.hpp"  
#include "nav2_util/node_utils.hpp"
#include "rosi_custom_controller/rosi_custom_controller.hpp"
#include "nav2_util/geometry_utils.hpp"

using std::hypot;
using std::min;
using std::max;
using std::abs;
using nav2_util::declare_parameter_if_not_declared;
using nav2_util::geometry_utils::euclidean_distance;

namespace rosi_custom_controller
{

/**
 * Find element in iterator with the minimum calculated value
 */
template<typename Iter, typename Getter>
Iter min_by(Iter begin, Iter end, Getter getCompareVal)
{
  if (begin == end) {
    return end;
  }
  auto lowest = getCompareVal(*begin);
  Iter lowest_it = begin;
  for (Iter it = ++begin; it != end; ++it) {
    auto comp = getCompareVal(*it);
    if (comp < lowest) {
      lowest = comp;
      lowest_it = it;
    }
  }
  return lowest_it;
}

void RosiCustomController::configure(
  const rclcpp_lifecycle::LifecycleNode::SharedPtr & node,
  std::string name, const std::shared_ptr<tf2_ros::Buffer> & tf,
  const std::shared_ptr<nav2_costmap_2d::Costmap2DROS> & costmap_ros)
{

  costmap_ros_ = costmap_ros;
  tf_ = tf;
  plugin_name_ = name;
  logger_ = node->get_logger();
  clock_ = node->get_clock();

  declare_parameter_if_not_declared(
    node, plugin_name_ + ".desired_linear_vel", rclcpp::ParameterValue(
      0.2));
  declare_parameter_if_not_declared(
    node, plugin_name_ + ".max_angular_vel", rclcpp::ParameterValue(
      1.0));
  declare_parameter_if_not_declared(
    node, plugin_name_ + ".transform_tolerance", rclcpp::ParameterValue(
      0.1));

  // Stanley exclusive
  declare_parameter_if_not_declared(
    node, plugin_name_ + ".k", rclcpp::ParameterValue(
      0.005));

  node->get_parameter(plugin_name_ + ".desired_linear_vel", desired_linear_vel_);
  node->get_parameter(plugin_name_ + ".max_angular_vel", max_angular_vel_);
  double transform_tolerance;
  node->get_parameter(plugin_name_ + ".transform_tolerance", transform_tolerance);
  transform_tolerance_ = rclcpp::Duration::from_seconds(transform_tolerance);

  // Stanley exclusive
  node->get_parameter(plugin_name_ + ".k", k_);

  global_pub_ = node->create_publisher<nav_msgs::msg::Path>("received_global_plan", 1);
}

void RosiCustomController::cleanup()
{
  RCLCPP_INFO(
    logger_,
    "Cleaning up controller: %s of type rosi_custom_controller::RosiCustomController",
    plugin_name_.c_str());
  global_pub_.reset();
}

void RosiCustomController::activate()
{
  RCLCPP_INFO(
    logger_,
    "Activating controller: %s of type rosi_custom_controller::RosiCustomController\"  %s",
    plugin_name_.c_str());
  global_pub_->on_activate();
}

void RosiCustomController::deactivate()
{
  RCLCPP_INFO(
    logger_,
    "Dectivating controller: %s of type rosi_custom_controller::RosiCustomController\"  %s",
    plugin_name_.c_str());
  global_pub_->on_deactivate();
}

/* Stanley Algorithm
In this modified implementation, the cross-track error is computed as the perpendicular 
distance from the robot to the path, and the heading error is calculated as the difference 
in orientation between the robot and the path. The desired angular velocity is then 
determined using the Stanley algorithm, where the gain k is a tuning parameter. 
Finally, the desired velocity commands are returned in a TwistStamped message.
*/
geometry_msgs::msg::TwistStamped RosiCustomController::computeVelocityCommands(
  const geometry_msgs::msg::PoseStamped & pose,
  const geometry_msgs::msg::Twist &)
{
  /* Stanley
  */
  // Transform the global plan to the local frame
  auto transformed_plan = transformGlobalPlan(pose);

  auto closest_pose_it = std::min_element(
    transformed_plan.poses.begin(), transformed_plan.poses.end(),
    [&](const auto & ps1, const auto & ps2) {
      double dist1 = euclidean_distance(pose.pose.position, ps1.pose.position);
      double dist2 = euclidean_distance(pose.pose.position, ps2.pose.position);
      return dist1 < dist2;
    });

  /**************************************************
   *  Compute the heading error (difference in orientation between the robot and the path)
  Heading error (HE):
  The heading error represents the difference in orientation between the robot 
  and the desired path. It is calculated as the difference in yaw angles between 
  the robot's orientation (theta_r) and the orientation of the path at the closest 
  point (theta_p).

  HE = theta_r - theta_p
  *
  **************************************************/
  tf2::Quaternion quat_robot, quat_path;
  tf2::fromMsg(pose.pose.orientation, quat_robot);
  tf2::fromMsg(closest_pose_it->pose.orientation, quat_path);
  double heading_error = tf2::getYaw(/*TODO: Quaternion*/.inverse() * /*TODO: Quaternion*/);

  /**************************************************
   *  Compute the cross-track error (perpendicular distance from the robot to the path)
  Cross-track error (CTE):
  The cross-track error represents the perpendicular distance from the robot to the desired path. 
  It is computed as the Euclidean distance between the robot's position (x_r, y_r) and the closest 
  point on the path (x_p, y_p).

  CTE = (x_r - x_p) * sin(theta_p) - (y_r - y_p) * cos(theta_p)
  where:
  (x_r, y_r) are the coordinates of the robot.
  (x_p, y_p) are the coordinates of the closest point on the path.
  theta_p is the orientation (yaw angle) of the path at the closest point.
  *
  **************************************************/
  // Compute the path heading
  double theta_p= /* TODO: Your code here */
  // Compute the cross-track error
  double cross_track_error = /* TODO: Your code here */

  /**************************************************
   *  Compute the desired angular velocity using the Stanley algorithm
  Desired angular velocity (omega):
  The desired angular velocity is computed using the following equation:
  omega = corrected heading error + cross track error correction term
  where:
  corrected heading error: k * heading error
  cross track error correction term: atan2(2 * k_ * y position of the closest pose, x position of the closest pose)

  This equation combines the heading error and the cross-track error to generate the desired 
  angular velocity for the robot's control. The atan2 function converts the CTE to an angle, 
  and k * HE provides a correction term based on the heading error. 
  *
  **************************************************/
  double corrected_heading_error = /* TODO: Your code here */
  double cross_track_error_correction_term = atan2(/* TODO: First arugment*/, /* TODO: Second argument*/);
  double desired_angular_vel = corrected_heading_error + cross_track_error_correction_term;

  // Account for the opposite angle
  // Check if the robot is on the opposite side of the path
  double opposite_angle = M_PI;  // Set the opposite angle value
  if (cross_track_error < 0) {
    desired_angular_vel += std::copysign(opposite_angle, desired_angular_vel);
  }

  // Limit the desired angular velocity to the maximum value
  desired_angular_vel = std::max(-max_angular_vel_, std::min(desired_angular_vel, max_angular_vel_));


  // Create and publish a TwistStamped message with the desired velocity
  geometry_msgs::msg::TwistStamped cmd_vel;
  cmd_vel.header.frame_id = pose.header.frame_id;
  cmd_vel.header.stamp = clock_->now();
  cmd_vel.twist.linear.x = desired_linear_vel_;
  cmd_vel.twist.angular.z = desired_angular_vel;
  return cmd_vel;

}

void RosiCustomController::setPlan(const nav_msgs::msg::Path & path)
{
  global_pub_->publish(path);
  global_plan_ = path;
}

nav_msgs::msg::Path
RosiCustomController::transformGlobalPlan(
  const geometry_msgs::msg::PoseStamped & pose)
{
  // Original mplementation taken fron nav2_dwb_controller

  if (global_plan_.poses.empty()) {
    throw nav2_core::PlannerException("Received plan with zero length");
  }

  // let's get the pose of the robot in the frame of the plan
  geometry_msgs::msg::PoseStamped robot_pose;
  if (!transformPose(
      tf_, global_plan_.header.frame_id, pose,
      robot_pose, transform_tolerance_))
  {
    throw nav2_core::PlannerException("Unable to transform robot pose into global plan's frame");
  }

  // We'll discard points on the plan that are outside the local costmap
  nav2_costmap_2d::Costmap2D * costmap = costmap_ros_->getCostmap();
  double dist_threshold = std::max(costmap->getSizeInCellsX(), costmap->getSizeInCellsY()) *
    costmap->getResolution() / 2.0;

  // First find the closest pose on the path to the robot
  auto transformation_begin =
    min_by(
    global_plan_.poses.begin(), global_plan_.poses.end(),
    [&robot_pose](const geometry_msgs::msg::PoseStamped & ps) {
      return euclidean_distance(robot_pose, ps);
    });

  // From the closest point, look for the first point that's further then dist_threshold from the
  // robot. These points are definitely outside of the costmap so we won't transform them.
  auto transformation_end = std::find_if(
    transformation_begin, end(global_plan_.poses),
    [&](const auto & global_plan_pose) {
      return euclidean_distance(robot_pose, global_plan_pose) > dist_threshold;
    });

  // Helper function for the transform below. Transforms a PoseStamped from global frame to local
  auto transformGlobalPoseToLocal = [&](const auto & global_plan_pose) {
      // We took a copy of the pose, let's lookup the transform at the current time
      geometry_msgs::msg::PoseStamped stamped_pose, transformed_pose;
      stamped_pose.header.frame_id = global_plan_.header.frame_id;
      stamped_pose.header.stamp = pose.header.stamp;
      stamped_pose.pose = global_plan_pose.pose;
      transformPose(
        tf_, costmap_ros_->getBaseFrameID(),
        stamped_pose, transformed_pose, transform_tolerance_);
      return transformed_pose;
    };

  // Transform the near part of the global plan into the robot's frame of reference.
  nav_msgs::msg::Path transformed_plan;
  std::transform(
    transformation_begin, transformation_end,
    std::back_inserter(transformed_plan.poses),
    transformGlobalPoseToLocal);
  transformed_plan.header.frame_id = costmap_ros_->getBaseFrameID();
  transformed_plan.header.stamp = pose.header.stamp;

  // Remove the portion of the global plan that we've already passed so we don't
  // process it on the next iteration (this is called path pruning)
  global_plan_.poses.erase(begin(global_plan_.poses), transformation_begin);
  global_pub_->publish(transformed_plan);

  if (transformed_plan.poses.empty()) {
    throw nav2_core::PlannerException("Resulting plan has 0 poses in it.");
  }

  return transformed_plan;
}

bool RosiCustomController::transformPose(
  const std::shared_ptr<tf2_ros::Buffer> tf,
  const std::string frame,
  const geometry_msgs::msg::PoseStamped & in_pose,
  geometry_msgs::msg::PoseStamped & out_pose,
  const rclcpp::Duration & transform_tolerance
) const
{
  // Implementation taken as is fron nav_2d_utils in nav2_dwb_controller

  if (in_pose.header.frame_id == frame) {
    out_pose = in_pose;
    return true;
  }

  try {
    tf->transform(in_pose, out_pose, frame);
    return true;
  } catch (tf2::ExtrapolationException & ex) {
    auto transform = tf->lookupTransform(
      frame,
      in_pose.header.frame_id,
      tf2::TimePointZero
    );
    if (
      (rclcpp::Time(in_pose.header.stamp) - rclcpp::Time(transform.header.stamp)) >
      transform_tolerance)
    {
      RCLCPP_ERROR(
        rclcpp::get_logger("tf_help"),
        "Transform data too old when converting from %s to %s",
        in_pose.header.frame_id.c_str(),
        frame.c_str()
      );
      RCLCPP_ERROR(
        rclcpp::get_logger("tf_help"),
        "Data time: %ds %uns, Transform time: %ds %uns",
        in_pose.header.stamp.sec,
        in_pose.header.stamp.nanosec,
        transform.header.stamp.sec,
        transform.header.stamp.nanosec
      );
      return false;
    } else {
      tf2::doTransform(in_pose, out_pose, transform);
      return true;
    }
  } catch (tf2::TransformException & ex) {
    RCLCPP_ERROR(
      rclcpp::get_logger("tf_help"),
      "Exception in transformPose: %s",
      ex.what()
    );
    return false;
  }
  return false;
}

}  // namespace rosi_custom_controller

// Register this controller as a nav2_core plugin
PLUGINLIB_EXPORT_CLASS(
  rosi_custom_controller::RosiCustomController, 
  nav2_core::Controller)