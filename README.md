# Recovery Behaviour Plugin (FOXY) Build Instuctions
## To use the answers provided in this repository, add the modified ROS 2 nav2 packages into the relevant directories

### 1. Move the following packages inside the folder,recovery_plugin_tutorial_modified_navigation_packages, into the navigation2 directory in your workspace
- nav2_behavior_tree
- nav2_bt_navigator
- nav2_msgs
- nav2_recoveries

### 2. Move the package inside the folder,recovery_plugin_tutorial_modified_navigation_packages, into the turtlebot3 directory in your workspace
- turtlebot3_navigation2

### 3. Build the workspace
